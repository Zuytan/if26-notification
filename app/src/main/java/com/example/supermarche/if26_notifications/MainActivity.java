package com.example.supermarche.if26_notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    final String CLASSIC_CHANNEL = "fr.utt.if26.notifications.CLASSIC_CHANNEL";
    final String IMPORTANT_CHANNEL = "fr.utt.if26.notification.IMPORTANT_CHANNEL";
    final String BUTTON_ACTION = "fr.utt.if26.notifications.BUTTON_ACTION";
    final int CLASSIC_NOTIFICATION = 001;
    final int CLICKABLE_NOTIFICATION = 002;
    final int BUTTON_NOTIFICATION = 003;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createClassicNotificationChannel();
        createImportantNotificationChannel();
        final Button b_classicNotification = findViewById(R.id.simple_notification);
        final Button b_clickableNotification = findViewById(R.id.clickable_notification);
        final Button b_buttonNotification = findViewById(R.id.button_notification);
        b_classicNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createClassicNotification();
            }
        });

        b_clickableNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createClickableNotification();
            }
        });

        b_buttonNotification.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                createButtonNotification();
            }
        });

    }

    /**
     * This method create a simple notification that only add a badge in the notification bar.
     */
    private void createClassicNotification(){
        String title = "A classic Notification";
        String content = "This is a classic notification with nothing special inside of it.";
        //The channel id is important for the API 26 version and later.
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CLASSIC_CHANNEL)
                //You can modify the small icon of the notification here, via resources
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        //We get the notification manager here to notify the system that a new notification is in coming
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        //Show the notification
        notificationManager.notify(CLASSIC_NOTIFICATION, mBuilder.build());
    }

    /**
     * This method create a notification that redirect to an activity when you click on it.
     */
    private void createClickableNotification(){
        // Create an intent for the "ResultActivity" class
        Intent intent = new Intent(this, ResultActivity.class);

        //If you want you can add specific flag to return to a special state in the application.
        //The pending Intent call the activity created in the intent and will create it when the intent will be fired.
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        //Set datas
        String title = "Clickable Notification";
        String content = "This notification is clickable and will redirect to an empty activity";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CLASSIC_CHANNEL)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        //We get the notification manager here to notify the system that a new notification is in coming
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        //Show the notification
        notificationManager.notify(CLICKABLE_NOTIFICATION, mBuilder.build());
    }

    /**
     * This method create a notification that react when the user tap on the button.
     */
    private void createButtonNotification(){
        //First, we have to create an intent that will redirect our message to a specific class
        Intent buttonIntent = new Intent(this, ButtonBroadcastReceiver.class);
        //We must define the action we want to send, it's important for the receiver, because it have to know that this message is for it.
        //Watch in the manifest to see the intent filter for ButtonBroadcastReceiver
        buttonIntent.setAction(BUTTON_ACTION);
        //Set the pending Intent as in the simple clickable button
        PendingIntent buttonPendingIntent =
                PendingIntent.getBroadcast(this, 0, buttonIntent, 0);

        //Set the datas
        String title = "Button Notification";
        String content = "This notification is only reacting when you tap on the button !";

        //Create the notification
        Notification mBuilder = new NotificationCompat.Builder(this, IMPORTANT_CHANNEL)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                //Add action create the button in the notification, with the specific text, icon (null here), and action corresponding
                .addAction(0, getString(R.string.button_notification),
                        buttonPendingIntent)
                .setAutoCancel(true)
                .build();
        //We get the notification manager here to notify the system that a new notification is in coming
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        //Show the notification
        notificationManager.notify(BUTTON_NOTIFICATION, mBuilder);

    }

    /**
     * This method create a channel. A channel is a group of notification.
     * It's really useful for the user if he want to manage kind of notification but not all of it.
     * It MUST be create for API 26+.
     * This method HAVE TO be called at the creation of the app.
     */
    private void createClassicNotificationChannel(){
        //Check if the current build is bigger than Android Oreo (API 26)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            //Set the channel's specification
            CharSequence name = getString(R.string.classic_channel_name);
            String description = getString(R.string.classic_channel_desc);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            //Create the notification Channel
            NotificationChannel channel = new NotificationChannel(CLASSIC_CHANNEL, name, importance);
            channel.setDescription(description);

            //Put the notification channel in the list of notification Manager.
            NotificationManager notifManager = getSystemService(NotificationManager.class);
            notifManager.createNotificationChannel(channel);
        }
    }

    /**
     * This method create another channel with another Importance, it ccan be usefull if you want to show heads up notification
     */
    private void createImportantNotificationChannel(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            //Set the channel's specification
            CharSequence name = getString(R.string.important_channel_name);
            String description = getString(R.string.important_channel_desc);
            int importance = NotificationManager.IMPORTANCE_HIGH;

            //Create the notification Channel
            NotificationChannel channel = new NotificationChannel(IMPORTANT_CHANNEL, name, importance);
            channel.setDescription(description);

            //Put the notification channel in the list of notification Manager.
            NotificationManager notifManager = getSystemService(NotificationManager.class);
            notifManager.createNotificationChannel(channel);
        }
    }

}
