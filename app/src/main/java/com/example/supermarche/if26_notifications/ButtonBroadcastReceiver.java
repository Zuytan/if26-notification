package com.example.supermarche.if26_notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ButtonBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("fr.utt.if26.notifications.BUTTON_ACTION")){
            Intent result = new Intent(context, ResultActivity.class);
            result.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(result);

        }
    }
}
